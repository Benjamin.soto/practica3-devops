variable "resource_group_name" {
  default = "myTFResourceGroup"
}

variable "resource_group_location" {
  default       = "westus"
  description   = "Location of the resource group."
}